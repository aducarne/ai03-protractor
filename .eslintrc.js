module.exports = {
    "extends": "google",
    "parser": "babel-eslint",
    "rules": {
        "linebreak-style": 0,
        "indent": ["error", 4],
        "max-len" : [2, 160, 4, {"ignoreUrls": true}]

    }
};