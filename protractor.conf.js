const HtmlReporter = require('protractor-beautiful-reporter');
const { SpecReporter } = require('jasmine-spec-reporter');
const path = require('path');
const auth = require('./environment.json');

exports.config = {
    allScriptsTimeout: 11000,
    //seleniumAddress: 'http://localhost:4444/wd/hub',

    // The port to start the selenium server on, or null if the server should
    // find its own unused port.
    seleniumPort: null,
    seleniumArgs: [],

    specs: ['./specs/*.spec.js'],

    params: {
        username: auth.username,
        password: auth.password
    },

    capabilities: {
        browserName: 'chrome',
        chromeOptions: {
            args: ['--window-size=1910,1080'],
            prefs: {
                download: {
                    prompt_for_download: false,
                    directory_upgrade: true,
                    default_directory: './reports-tmp'
                }
            }
        }
    },

    // A base URL for your application under test. Calls to protractor.get()
    // with relative paths will be prepended with this.
    baseUrl: 'https://webapplis.utc.fr/ent/',
    directConnect: true,

    framework: 'jasmine',

    onPrepare: function() {
        jasmine.getEnv().addReporter(new SpecReporter({ spec: { displayStacktrace: true } }));

        // Add a screenshot reporter:
        jasmine.getEnv().addReporter(
            new HtmlReporter({
                preserveDirectory: false,
                takeScreenShotsOnlyForFailedSpecs: true,
                screenshotsSubfolder: 'images',
                jsonsSubfolder: 'jsons',
                baseDirectory: 'reports-tmp',
                clientDefaults: {
                    columnSettings: {
                        displaySessionId: false
                    }
                },
                pathBuilder: function pathBuilder(spec, descriptions, results, capabilities) {
                    const currentDate = new Date();
                    const day = currentDate.getDate();
                    const month = currentDate.getMonth() + 1;
                    const year = currentDate.getFullYear();
                    const time = currentDate.getHours();

                    const validDescriptions = descriptions.map(function(description) {
                        return description.replace('/', '@');
                    });

                    return path.join(year + '-' + month + '-' + day + '-' + time, validDescriptions.join('-'));
                }
            }).getJasmine2Reporter()
        );
    },
    jasmineNodeOpts: {
        showColors: true,
        defaultTimeoutInterval: 30000,
        print: function() {}
    }
};
