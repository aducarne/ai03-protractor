# AI03 Project : Protractor
## Install Guide
Prerequisites : NodeJS
### Installing Protractor
Clone this repo and run

    npm install -g protractor
### Updating Web-driver
    webdriver-manager update
___
### Getting Dev Environment
Run NPM install to get all the necessaries dev packages.
## How to use
Simply run webdriver-manager start to start the server.
Then run the config file with Protractor `protractor protractor.conf.js`
