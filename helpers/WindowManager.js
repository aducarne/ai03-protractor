class WindowManager {
    switchToNextTab(closeCurrentTab) {
        browser.getAllWindowHandles().then(handles => {
            if (closeCurrentTab) browser.driver.close();
            browser.driver.switchTo().window(handles[1]);
        });
    }
}

module.exports = new WindowManager();
