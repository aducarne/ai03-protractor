class Login {
    getUsername() {
        return $('#username');
    }

    getPassword() {
        return $('#password');
    }

    getSubmitButton() {
        return element(by.name('Submit1'));
    }
}

module.exports = new Login();
