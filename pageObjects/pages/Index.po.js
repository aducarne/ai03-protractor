class Index {
    getTrombinoscopeButton() {
        return $('#btnTrombi');
    }

    getEtuDirectoryButton() {
        return element(by.cssContainingText('a', 'Mon dossier étudiant'));
    }

    goToEtuDirectory() {
        this.getEtuDirectoryButton().click();
    }
}

module.exports = new Index();
