class Trombinoscope {
    getLastNameInput() {
        return element(by.id('sindiv:LeNom'));
    }

    getFirstNameInput() {
        return element(by.id('sindiv:LePrenom'));
    }

    getSubmitButton() {
        return element(by.id('sindiv:j_idt20'));
    }

    getResultName() {
        return element.all(by.className('TrombiTitre fn')).get(0);
    }

    getAllResultsName() {
        return element.all(by.className('TrombiTitre fn'));
    }
}

module.exports = new Trombinoscope();
