const Index = require('../pageObjects/pages/index.po');
const Trombinoscope = require('../pageObjects/elements/Trombinoscope.po');
const EC = protractor.ExpectedConditions;

describe('Trombinoscope', () => {
    beforeAll(() => {
        browser.ignoreSynchronization = true;
        browser.get('index.jsf');
    });

    it('should expand the Trombinoscope', () => {
        Index.getTrombinoscopeButton().click();
    });

    it('should type in Sallak', function() {
        let nameInput = Trombinoscope.getLastNameInput();
        browser.wait(EC.elementToBeClickable(nameInput), 2000);
        nameInput.sendKeys('Sallak');
        Trombinoscope.getSubmitButton().click();
    });

    it('should find Mohamed Sallak', function() {
        browser.sleep(2000);
        expect(Trombinoscope.getResultName().getText()).toBe('SALLAK Mohamed');
    });
});
