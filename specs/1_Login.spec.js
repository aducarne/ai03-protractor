const Login = require('../pageObjects/pages/Login.po.js');
const EC = protractor.ExpectedConditions;

describe('Authentication', () => {
    beforeAll(function() {
        browser.ignoreSynchronization = true;
        browser.get('index.jsf');
    });

    it('should enter username', () => {
        let username = Login.getUsername();
        username.sendKeys(browser.params.username);
        expect(username.getAttribute('value')).toBe(browser.params.username);
    });

    it('should enter password', () => {
        let password = Login.getPassword();
        password.sendKeys(browser.params.password);
        expect(password.getAttribute('value')).toBe(browser.params.password);
    });

    it('should submit and be logged in', () => {
        Login.getSubmitButton().click();
        browser.wait(EC.urlContains('https://webapplis.utc.fr/ent/index.jsf'), 10000)
    });
});
