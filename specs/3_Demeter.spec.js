const Index = require('../pageObjects/pages/Index.po');
const Demeter = require('../pageObjects/pages/Demeter.po');
const WindowManager = require('../helpers/WindowManager');
const EC = protractor.ExpectedConditions;

describe('Certficate Download', () => {
    beforeAll(() => {
        browser.ignoreSynchronization = true;
        browser.get('index.jsf');
    });

    it('should click the button to the etu directory server', () => {
        Index.goToEtuDirectory();
    });

    it('should be on demeter server', () => {
        WindowManager.switchToNextTab(true);
        browser.wait(EC.urlContains(Demeter.getBaseURI()), 10000);
        browser.sleep(5000);
    });
});
